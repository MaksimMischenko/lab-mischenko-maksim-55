import React, { Component } from 'react';
import './App.css';
import TopBottom from './components/Burger/TopBottom';
import BreadBottom from './components/Burger/BreadBottom';
import Filling from "./components/Burger/Fillings";
import Ingredients from "./components/Ingredients/Ingredients";

const INGREDIENTS =[
    {name: 'Meat', price: 50},
    {name: 'Salad', price: 5},
    {name: 'Cheese', price: 20},
    {name: 'Bacon', price: 30}
];

class App extends Component {
    state = {
        ingredients: [
            {name: 'Salad', count: 0},
            {name: 'Meat', count: 0},
            {name: 'Cheese', count: 0},
            {name: 'Bacon', count: 0}
        ],
        totalPrice: 20
    };

    createFilling = () => {
        let fillArr = [];

        for (let i = 0; i < this.state.ingredients.length; i++){
            for (let j = 0; j < this.state.ingredients[i].count; j++){
                fillArr.push(<Filling key={""+i+''+j} name={this.state.ingredients[i].name}/>)
            }
        }

        return fillArr;
    };
    addIngredient = (name) => {
        let price = 0;
        for (let i = 0; i < INGREDIENTS.length; i++){
            if (INGREDIENTS[i].name === name) {
                price = INGREDIENTS[i].price;
            }
        }
        let total = this.state.totalPrice;
        total+= price;

        const stateIngredients = [...this.state.ingredients];
        for(let i = 0; i < stateIngredients.length; i++) {
            if(stateIngredients[i].name === name) {
                stateIngredients[i].count++
            }
        }



        this.setState({
            totalPrice: total,
            ingredients: stateIngredients
        })
    };


    removeIngredient = (name) => {
        const stateIngredients = [...this.state.ingredients];
        for(let i = 0; i < stateIngredients.length; i++) {
            if(stateIngredients[i].name === name) {
                stateIngredients[i].count--
            }
        }
        let  price = 0;
        for (let i = 0; i < INGREDIENTS.length; i++){
            if (INGREDIENTS[i].name === name) {
                price = INGREDIENTS[i].price;
            }
        }
        let total = this.state.totalPrice;
        total-= price;


        this.setState({
            totalPrice: total,
            ingredients: stateIngredients
        })
    };

    render() {
        return (
            <div className="App">
                <div className='ingredients'> Ингредиенты
                    <Ingredients remove={(name) => this.removeIngredient(name)} ing={this.state.ingredients} click={(name)=>{this.addIngredient(name)}}/>
                </div>
                <div className="Burger"> Бургер
                    <TopBottom/>
                    {this.createFilling()}
                    <BreadBottom/>
                    spanЦена: {this.state.totalPrice} Сом
                </div>
            </div>
        );
    }


}

export default App;