import React from 'react';
import './Ingredients.css';

const Ingredients = (props) =>{
    return(
        props.ing.map((ingredients, index) =>{
            return(
                <div key={index} className='Form'>
                    <button className='Okay' onClick={() => props.click(ingredients.name)}>✔</button>
                    <h5> {ingredients.name} </h5>
                    <p> Кол: {ingredients.count}</p>
                    {ingredients.count !== 0 ?
                        <button className='Del' onClick={() => props.remove(ingredients.name)}>Delete</button>
                        : null}
                </div>
            )
        })

    )
};

export default Ingredients